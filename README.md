# thehobbyapp

a [Sails v1](https://sailsjs.com) application


### Links

+ [Sails framework documentation](https://sailsjs.com/get-started)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)


### Version info

This app was originally generated on Mon Jul 15 2019 10:12:09 GMT+0100 (W. Central Africa Standard Time) using Sails v1.2.3.

<!-- Internally, Sails used [`sails-generate@1.16.13`](https://github.com/balderdashy/sails-generate/tree/v1.16.13/lib/core-generators/new). -->



<!--
Note:  Generators are usually run using the globally-installed `sails` CLI (command-line interface).  This CLI version is _environment-specific_ rather than app-specific, thus over time, as a project's dependencies are upgraded or the project is worked on by different developers on different computers using different versions of Node.js, the Sails dependency in its package.json file may differ from the globally-installed Sails CLI release it was originally generated with.  (Be sure to always check out the relevant [upgrading guides](https://sailsjs.com/upgrading) before upgrading the version of Sails used by your app.  If you're stuck, [get help here](https://sailsjs.com/support).)
-->


#### Installation
Navigate to the application root directory and run the command below
` npm install `

#Take note of this
Add `.env` to the root folder and fill in your environment variables as follows. This are really important for this application to work.
`
###Json web token secret
JWT_SECRET = xxxxx
###Twilio Api key and token
TWILIO_ACCOUNT_SID = xxxxx
TWILIO_AUTH_TOKEN = xxxxx
###Mailgun api key
MAILGUN_API_KEY = xxxxx
###Twilio Phone number
TWILIO_PHONE_NUMBER = xxxxx
###Mongodb URL
MONGODB_URL =xxxxx
###Numerify api key for phone number verification
NUMVERIFY_API_KEY = xxxxx
`

#Technologies Used
1. Mongodb
2. Sailsjs,
3. Twilio,
4. Mailgun,
5. Json web token,
6. Passport,
7. webpack,
8. react

#Start application
Run `node app.js or sails lift`


Vioala!! , this application should be running on `http://locahost:1337`

##NOTE:: This step is for running this application in development

